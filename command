#!/bin/bash

RETVAL="0"
DIRNAME=`dirname $0`

function log()
{
  echo "[universe] ${1}"
}

function run()
{
  log 'Launch web server'
  php -S localhost:8000 -t app/
  RETVAL="0"
}

function usage()
{
   echo "Usage: $0 {run}"
   RETVAL="2"
}

for ARGUMENT in $@
do
  RETVAL="0"
  case $ARGUMENT in
  run) run ;;
  *) usage ;;
  esac
  if [ "$RETVAL" -ne 0 ]; then
    break
  fi
done

exit $RETVAL
