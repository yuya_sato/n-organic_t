<?php
/* EBiS Redirect PG Ver3.2
 * @author Lockon Co.,Ltd
 * @version $Revision$
 **/

/**
 *  settings
 */

    /** EBiS argument */
    //define("ARG", "your-ebis-param");.
    define("ARG", "NxGGfPWq");

    /** redirect url  (ex. http://www.yourdomain.jp/ ) */
    define("DEFAULT_URL", "http://cosme-labo.com");


/**
 *  system-params
 */
    /** host name */
    define("EBiS_HOST_NAME", "ac.ebis.ne.jp");
    define("EBiS_HOST_NAME_SSL", "ac.ebis.ne.jp");

    /** timeout */
    define("EBiS_TIME_OUT", 3);

    /** AD_ID */
    if ( isset($_GET['aid']) ) $ebis_ai = $_GET['aid'];
    if ( isset($_GET['ai']) ) $ebis_ai = $_GET['ai'];
    define( "AD_ID", htmlspecialchars(urldecode($ebis_ai), ENT_QUOTES) );

    /** check_file */
    define("CHECK_URL", '/check/'.ARG.'.dat');

    /** debug mode (0:false 1:true) */
    define("SETUP_FLAG", 1);

    /** password */
    define("SETUP_PW", 'passwd');

/**
 *   program
 */


//-------- main routine
if ( SETUP_FLAG == 1 && isset($_GET['ebis_debug']) && $_GET['ebis_debug'] == SETUP_PW ){
   echo "<h2>checking</h2><br><br>";
   echo "Your PHP version is ".PHP_VERSION."<br>";
   if ( $error = do_check() ){
       echo "<font color='red'>Warning</font><br>";
        foreach ($error as $key=>$val) {
            echo "[$key] $val<br>";
        }
   } else {
        echo "check success";
   }
   exit;
}

do_redirect();



//-------- function

/** check */
function do_check(){
    $error = array();
    if ( ! ini_get("allow_url_fopen") ){
        $error['php_ini'] = "allow_url_fopen check error.";
    }

    list($protocol, $host) = getProtocolUrl();
    $url = $protocol . $host . CHECK_URL;
    if ( ! fopen( $url, 'r') ){
        $error['EBiS_SERVER_NAME'] = $url." read error.";
    } else {
        echo "read success " .$url."<br>\n";
    }

    return $error;
}


/** redirect */
function do_redirect(){
    $flag = true;
    list($protocol, $host) = getProtocolUrl();

    // EBiS check flag
    if ($protocol == 'http://') {
        $fp = @fsockopen($host, 80, $errno, $errstr, EBiS_TIME_OUT);
    } else {
        $fp = @fsockopen("ssl://".$host, 443, $errno, $errstr, EBiS_TIME_OUT);
    }

    // dead or TCP/IP connection timeout
    if ( $fp === false ) {
        // hardware dead
        $flag = false;
        $redirect_url = DEFAULT_URL;

    } else {
        // TCP/IP OK

        fwrite($fp, "GET ".CHECK_URL." HTTP/1.0\r\n\r\n");

        // setting response timeout
        stream_set_blocking( $fp, TRUE );
        stream_set_timeout($fp, EBiS_TIME_OUT);

        $info = stream_get_meta_data($fp);
        while( !feof($fp) && !$info['timed_out'] ){
            $txt = fgets($fp, 2000);
            $info = stream_get_meta_data($fp);
        }

        if ( $txt == "1" ){
            // alive
            $redirect_url = $protocol.$host."/tr_set.php?argument=" .ARG ."&ai=" .AD_ID;
        } else {
            // software dead
            $flag = false;
            $redirect_url = DEFAULT_URL;
        }
        fclose($fp);
    }
    $query = get_link_url();
    if ( $query && $flag === true ) $redirect_url .= $query;

    header( "Cache-Control: private, max-age=0, no-cache" );
    header( "Location: ". $redirect_url );

    unset($fp);
    exit;
}


/**
 *  make query
 *
 *  @return string query
 */
function get_link_url(){
    $arrGet = array( "aid",
                    );

    $referrer = "";

    $data = explode("&", $_SERVER["QUERY_STRING"]);
    if ( is_array($data) && strlen($_SERVER["QUERY_STRING"])>0 ){
        foreach($data as $line){
            $tmp = explode("=", $line, 2);
            $query[$tmp[0]] = $tmp[1];
        }
    }

    if ( is_array($query) ){
        foreach ($query as $key => $value) {
            if ( ! in_array( strtolower($key), array_map( "strtolower", $arrGet) ) ) {
                $referrer.= "&". htmlspecialchars($key, ENT_QUOTES)."=". htmlspecialchars($value, ENT_QUOTES);
            }
        }
    }
    return $referrer;
}

/**
 *  get Protocol of request
 *
 *  @return string protocol http or https
 */
function getProtocolUrl() {
    return (isset($_SERVER['HTTPS']) && (strtolower($_SERVER['HTTPS']) == 'on' || $_SERVER['HTTPS'] == 1))
    ? array('https://' , EBiS_HOST_NAME_SSL)
    : array('http://'  , EBiS_HOST_NAME);
}
?>
