<!DOCTYPE html>
<html lang="ja">
<head>
  <?php include("../views/head.php"); ?>
  <link rel="stylesheet" type="text/css" media="all" href="/public/css/search/style.css">
  <link rel="stylesheet" type="text/css" media="all" href="/public/css/form/style.css">
  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5HZ565Q');</script>
<!-- End Google Tag Manager -->
</head>
<body>
  <script id="tagjs" type="text/javascript">
  (function () {
    var tagjs = document.createElement("script");
    var s = document.getElementsByTagName("script")[0];
    tagjs.async = true;
    tagjs.src = "//s.yjtag.jp/tag.js#site=1FNnqKh,7LkYJz3";
    s.parentNode.insertBefore(tagjs, s);
  }());
</script>
<noscript>
  <iframe src="//b.yjtag.jp/iframe?c=1FNnqKh,7LkYJz3" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
</noscript>
  <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HZ565Q"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php

  $voteParams = $_GET;
  $priceParams = $_GET;
  $capacityParams = $_GET;
  if ($_GET['sort'] == 'vote' && (!isset($_GET['order']) || (isset($_GET['order']) && $_GET['order'] != "asc"))) {
    $voteParams = array_merge($voteParams, array('order' => 'asc'));
  } else {
    $voteParams = array_merge($voteParams, array('order' => 'desc'));
  }
  $voteParams = array_merge($voteParams, array('sort' => 'vote'));
  $voteButtonParams = join('&', array_map(function ($key, $value) {
    return $key . '=' . $value;
  }, array_keys($voteParams), array_values($voteParams)));


  if ($_GET['sort'] == 'price' && (!isset($_GET['order']) || (isset($_GET['order']) && $_GET['order'] != "asc"))) {
    $priceParams = array_merge($priceParams, array('order' => 'asc'));
  } else {
    $priceParams = array_merge($priceParams, array('order' => 'desc'));
  }
  $priceParams = array_merge($priceParams, array('sort' => 'price'));
  $PriceButtonParams = join('&', array_map(function ($key, $value) {
    return $key . '=' . $value;
  }, array_keys($priceParams), array_values($priceParams)));

  if ($_GET['sort'] == 'capacity' && (!isset($_GET['order']) || (isset($_GET['order']) && $_GET['order'] != "asc"))) {
    $capacityParams = array_merge($capacityParams, array('order' => 'asc'));
  } else {
    $capacityParams = array_merge($capacityParams, array('order' => 'desc'));
  }
  $capacityParams = array_merge($capacityParams, array('sort' => 'capacity'));
  $CapacityButtonParams = join('&', array_map(function ($key, $value) {
    return $key . '=' . $value;
  }, array_keys($capacityParams), array_values($capacityParams)));
?>

<?php
  $url = "../json/app.json";
  $json = file_get_contents($url);
  $json = mb_convert_encoding($json, 'UTF8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');
  $cosmes = json_decode($json,true);

  switch ($_GET['sort']) {
    case 'vote':
      foreach ($cosmes as $key => $value) {
        $sortValue[$key] = $value['vote'];
      }
      array_multisort($sortValue, isset($_GET['order']) && $_GET['order'] == 'asc' ? SORT_ASC : SORT_DESC, $cosmes);
      break;
    case 'price':
      foreach ($cosmes as $key => $value) {
        $sortValue[$key] = $value['price'];
      }
      array_multisort($sortValue, isset($_GET['order']) && $_GET['order'] == 'asc' ? SORT_ASC : SORT_DESC, $cosmes);
      break;
    case 'capacity':
      foreach ($cosmes as $key => $value) {
        $sortValue[$key] = $value['capacity'];
      }
      array_multisort($sortValue, isset($_GET['order']) && $_GET['order'] == 'asc' ? SORT_ASC : SORT_DESC, $cosmes);
      break;

    default:
      break;
  }

  $filterValues = array_filter($_GET, function($value) {
    return !preg_match("/_all/", $value);
  });

  $filteredCosmes = array_filter($cosmes, function ($cosme) use ($filterValues) {
    if (array_key_exists('vote', $filterValues)) {
      if ($filterValues['vote'] < $cosme['vote']) {
        return false;
      }
    }
    if (array_key_exists('price', $filterValues)) {
      if ($filterValues['price'] < $cosme['price']) {
        return false;
      }
    }
    if (array_key_exists('capacity', $filterValues)) {
      if ($filterValues['capacity'] < $cosme['capacity']) {
        return false;
      }
    }
    if (array_key_exists('trial', $filterValues)) {
      if (($filterValues['trial'] == "true") != $cosme['trial']) {
        return false;
      }
    }
    if (array_key_exists('subscription', $filterValues)) {
      if (($filterValues['subscription'] == "true") != $cosme['subscription']) {
        return false;
      }
    }
    if (array_key_exists('postage', $filterValues)) {
      if (($filterValues['postage'] == "true") != $cosme['postage']) {
        return false;
      }
    }
    if (array_key_exists('returned', $filterValues)) {
      if (($filterValues['returned'] == "true") != $cosme['returned']) {
        return false;
      }
    }

    return true;
  }); ?>



  <div class="wrapper">
    <header class="header">
      <p class="box-header text-header">Cosme Labo</p>
    </header>

    <div class="container">
      <p class="result-title">検索結果</p>
      <div class="search-result-wrapper">
        <div class="content-select-box">
          <a href="/search?<?php echo $voteButtonParams; ?>" class="sort-button <?php echo $_GET['sort'] == 'vote' ? 'active' : ''?>">獲得票<span><?php echo $_GET['sort'] == 'vote' && isset($_GET['order']) && $_GET['order'] == 'asc' ? "▲" :"▼" ?></span></a>
          <a href="/search?<?php echo $PriceButtonParams; ?>" class="sort-button <?php echo $_GET['sort'] == 'price' ? 'active' : ''?>">価格<span><?php echo $_GET['sort'] == 'price' && isset($_GET['order']) && $_GET['order'] == 'asc' ? "▲" :"▼" ?></span></a>
          <a href="/search?<?php echo $CapacityButtonParams; ?>" class="sort-button <?php echo $_GET['sort'] == 'capacity' ? 'active' : ''?>">内容量<span><?php echo $_GET['sort'] == 'capacity' && isset($_GET['order']) && $_GET['order'] == 'asc' ? "▲" :"▼" ?></span></a>
        </div>
        <div class="item-list-wrapper">
          <?php if (empty($filteredCosmes)) { ?>
            <p class="notfound-message">みつかりませんでした。</p>
          <?php } else { ?>
            <?php foreach($filteredCosmes as $cosme) { ?>
              <?php include("../views/searchItem.php"); ?>
            <?php } ?>
          <?php } ?>
        </div>

      </div>

    </div>

    <?php include("../views/footer/default.php"); ?>

  </div>
</body>
</html>
