<!DOCTYPE html>
<html lang="ja">
<head>
  <?php include("../../views/head.php"); ?>
  <link rel="stylesheet" type="text/css" media="all" href="/public/css/articles/style.css">
</head>
<body>
  <div class="wrapper">
    <header class="header">
      <p id="top" class="box-header text-header">CosmeMagazine</p>
    </header>

    <div class="container">
      <div class="contents-headline">
        <div class="contents-headline-image"><img src="/public/image/articles/170630/img-00.jpg" alt=""></div>
        <div class="contents-headline-text">
          <h2 class="contents-headline-title">感動の保湿力！たった2ステップですっぴんに自信がもてるオーガニックコスメ！</h2>
          <div class="contents-headline-option">
            <p class="contents-headline-status">
              <span>PR</span>
              <time>2017.06.30</time>
            </p>
          </div>
        </div>
      </div>
      <div class="box-article">
        <p class="article-item article-item_text">美容ライターの渡辺と申します。<br><br>私自身、有名なブランド名に惹かれ、デパコスを愛用していた時期もあったのですが、毎回買い換える時期になるとお金がかかりすぎて続けられず・・・ドラッグストアで適当に買うようになり、化粧ノリがいまいちになっていました。<br><br>そこで、ちゃんとスキンケアを見直そう！と思い初めて、肌にも優しいと言われるオーガニックコスメを使ってみました。</p>
        <p class="article-item article-item_text"><span class="item-text-strong">肌にのせた瞬間にわかった「これ、良い！」</span><br>とろみのあるテクスチャなのに、すーっと浸透。<span class="item-text-strong">超乾燥肌が、1日中潤い続き！</span>化粧崩れも感じなくなりました。香りは上品に甘い柑橘系の香りで、アロマ効果も期待できそう！</p>
        <p class="article-item article-item_text">しかも！ローション&セラムのたった2本でスキンケアが完了するので、2ステップで手軽に時短もできる！そんなとてもオススメなN organicを紹介します！</p>
      </div>
      <div class="box-article">
        <h3 class="article-item_headline">すっぴん肌に自信が・・！</h3>
        <p class="article-item article-item_text"><img src="/public/image/articles/170630/img-01.jpg" alt=""></p>
        <p class="article-item article-item_text">とても乾燥肌な私。<br>毛穴が開きっぱなしで、最近ハリも気になってくる。ファンデや化粧でごまかしごまかしやってきました…。<br><br>でも、そんな私が、<span class="item-text-strong">今はすっぴんを公開できるようになった</span>のです！<br><br>ついに出会ってしまった、運命のオーガニックコスメのおかげです。</p>
        <p class="article-item article-item_text"><img src="/public/image/articles/170630/img-02.jpg" alt=""></p>
        <p class="article-item article-item_text">
          <a class="item-button_anchor siteLink" href="https://n-organic.com/ad_groups/articles-170631?<?php echo isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] . '&' : ''; ?>partner=cl&menu=article&lp=170631" target="_blank">N organicをもっと詳しく知りたい♡</a>
        </p>
      </div>
      <div class="box-article">
        <h3 class="article-item_headline">気になる使用感を本気でレビュー！</h3>
        <p class="article-item article-item_text">まずは、ローションで栄養をたっぷり補給！<br>オーガニックコスメにありがちな、さらっとしていて保湿力のなさそうなものではなく、とろみのあるテクスチャーなのに、スーッと浸透。<br>やさしくおしこむように、深呼吸しながら。手に吸い付くもち肌に感動！</p>
        <p class="article-item article-item_image"><img src="/public/image/articles/170630/img-03.jpg" alt=""></p>
        <p class="article-item article-item_text">次に美容セラムで肌にぴったりと蓋を。<br>ローション同様、べたつかないのにしっかり潤いの蓋をしてくれる。</p>
        <p class="article-item article-item_image"><img src="/public/image/articles/170630/img-04.jpg" alt=""></p>
        <p class="article-item article-item_text">これ1本で、乳液・クリーム・美容液・パック・化粧下地の5役！</p>
        <p class="article-item article-item_image"><img src="/public/image/articles/170630/img-05.jpg" alt=""></p>
        <p class="article-item article-item_text">もうオーガニックコスメは物足りないというイメージは崩れました。</p>
        <p class="article-item article-item_text">そんなN organicをしばらく使っていると･･･<br><br>「赤ちゃんみたいにもちもちな肌だよね」<br><br>「化粧品変えた？最近肌がつやつやだよね」<br><br>「ついにエステにでも通い始めたの？」<br><br>友達からこんな風に言われ始めました・・・</p>
        <p class="article-item article-item_text">
          <a class="item-button_anchor siteLink" href="https://n-organic.com/ad_groups/articles-170631?<?php echo isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] . '&' : ''; ?>partner=cl&menu=article&lp=170631" target="_blank">N organicをもっと詳しく知りたい♡</a>
        </p>
      </div>
      <div class="box-article">
        <h3 class="article-item_headline">なんでこんなにすごいの？</h3>
        <p class="article-item article-item_text">それは、N organicが<span class="item-text-strong">12種類の高品質なオーガニック成分で作られ</span>、乾燥による小ジワやハリ不足による毛穴の開き、カサカサの肌荒れ等、様々な悩みに有用な成分がたくさん配合されているのです。</p>
        <p class="article-item article-item_image"><img src="/public/image/articles/170630/img-06.jpg" alt=""></p>
        <p class="article-item article-item_text">特に「アルガンオイル」、「発酵ローズはちみつ」、「ホホバ」が贅沢に配合されているのがポイント。<br><br>アルガンオイルは、肌のキメを整え、保湿はもちろんアンチエイジングも期待が出来る魔法のオイル。<br><br>発酵ローズはちみつは、希少価値が高く、はちみつ特有の高い保湿効果に加えて、皮脂の過剰な分泌を抑えてくれる優れもの。<br><br>さらにホホバオイルは肌馴染みが良く、水分を逃がさずにやさしく肌をケアできるのです。</p>
      </div>
      <div class="box-article">
        <h3 class="article-item_headline">気になる値段は…？</h3>
        <p class="article-item article-item_text">こんなに<span class="item-text-strong">高品質</span>なのに、<span class="item-text-strong">コスパ良い価格</span>なんです！</p>
        <p class="article-item article-item_image"><img src="/public/image/articles/170630/img-07.jpg" alt=""></p>
        <p class="article-item article-item_text">なんでそんなことができているかというと、余分なコストは徹底的にカットして、WEBのみの発売・工場からの直送で店舗代・人件費・輸送代を大幅カットしたからなんです。</p>
        <p class="article-item article-item_text">さらに！<br><br>肌に合うか試したい人には、うれしいトライアルセットがあります！<br><br>定価60%OFFの1480円（税抜）でお試しできます！さらに定期購入すると、毎回25%OFFで購入できる！これはお得！</p>
        <p class="article-item article-item article-item_convertion">
          <img src="/public/image/articles/170630/img-08.png" alt="" class="item_convertion_background">
          <a class="item_convertion_button siteLink" href="https://n-organic.com/ad_groups/articles-170631?<?php echo isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] . '&' : ''; ?>partner=cl&menu=article&lp=170631" target="_blank"><img src="/public/image/articles/170630/img-09.png" alt=""></a>
        </p>
      </div>
      <div class="box-article">
        <h3 class="article-item_headline">実は読モがインスタにあげて話題に！</h3>
        <p class="article-item article-item_text">インスタグラムで検索してみたらたくさんモデルの人たちが写真をあげていました。</p>
        <p class="article-item article-item_text item-text-emphasis">STORY読モで2児のママ<br>亜矢さん</p>
        <p class="article-item article-item_text">
          <a class="item-box_anchor" href="https://www.instagram.com/p/BUDOUhfg52s/?taken-by=aya_green1010&hl=ja">
            <img src="https://scontent-nrt1-1.cdninstagram.com/t51.2885-15/e35/18382071_1154966974648443_4299493074838487040_n.jpg" alt="">
            <span class="item-text_anchor">@aya_green1010</span>
          </a>
        </p>
        <p class="article-item article-item_text item-text-emphasis item-text-emphasis">VERY読モで1児のママ<br>前田広美さん</p>
        <p class="article-item article-item_text">
          <a class="item-box_anchor" href="https://www.instagram.com/p/BVHMgNRl44q/?taken-by=hiron953&hl=ja">
            <img src="https://scontent-nrt1-1.cdninstagram.com/t51.2885-15/s1080x1080/e35/18889187_446119479080540_216233624620498944_n.jpg" alt="">
            <span class="item-text_anchor">@hiron953</span>
          </a>
        </p>
        <p class="article-item article-item_text item-text-emphasis item-text-emphasis">MAQUIA読モ<br>橋本香織さん</p>
        <p class="article-item article-item_text">
          <a class="item-box_anchor" href="https://www.instagram.com/p/BTgnkCClVqZ/?taken-by=haana0424&hl=ja">
            <img src="https://scontent-nrt1-1.cdninstagram.com/t51.2885-15/e35/18160248_282274882183961_986560121104498688_n.jpg" alt="">
            <span class="item-text_anchor">@haana0424</span>
          </a>
        </p>
        <p class="article-item article-item_text item-text-emphasis item-text-emphasis">VOCEやMOREで活躍するモデル<br>高山直子さん</p>
        <p class="article-item article-item_text">
          <a class="item-box_anchor" href="https://www.instagram.com/p/BT80LVbANbR/?taken-by=nao_70koro&hl=ja">
            <img src="https://scontent-nrt1-1.cdninstagram.com/t51.2885-15/e35/18380295_696997750487913_5659196883717849088_n.jpg" alt="">
            <span class="item-text_anchor">@nao_70koro</span>
          </a>
        </p>
        <p class="article-item article-item_text item-text-emphasis item-text-emphasis">美STで特集された人気のインスタグラマー<br>山田あきこさん</p>
        <p class="article-item article-item_text">
          <a class="item-box_anchor" href="https://www.instagram.com/p/BU3kR56F2Ax/?taken-by=akko3839&hl=ja">
            <img src="https://scontent-nrt1-1.cdninstagram.com/t51.2885-15/e35/18812801_1497863723612528_8118567514721484800_n.jpg" alt="">
            <span class="item-text_anchor">@akko3839</span>
          </a>
        </p>
      </div>
      <div class="box-article">
        <h3 class="article-item_headline">まとめると</h3>
        <ul class="article-item article-item_text article-item_lists">
          <li class="item-text_list">高品質なのにコスパよい<span class="item-text-emphasis">オーガニックスキンケア</span></li>
          <li class="item-text_list"><span class="item-text-emphasis">12種のオーガニック成分</span>で様々な悩みにアプローチ</li>
          <li class="item-text_list"><span class="item-text-emphasis">アルガンオイル</span>や<span class="item-text-emphasis">発酵ローズはちみつ</span>配合で高保湿</li>
          <li class="item-text_list"><span class="item-text-emphasis">簡単2ステップ</span>でスキンケア完了</li>
          <li class="item-text_list">ライン使いしても、<span class="item-text-emphasis">1万円以下</span>で長く愛用</li>
        </ul>
        <p class="article-item article-item_text"><br><br>そんなN organicで、素肌に自信を。<br><br>ぜひあなたもお試しください！</p>
        <p class="article-item article-item_convertion">
          <img src="/public/image/articles/170630/img-08.png" alt="" class="item_convertion_background">
          <a class="item_convertion_button siteLink" href="https://n-organic.com/ad_groups/articles-170631?<?php echo isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] . '&' : ''; ?>partner=cl&menu=article&lp=170631" target="_blank"><img src="/public/image/articles/170630/img-09.png" alt=""></a>
        </p>
      </div>
    </div>
    <?php include("../../views/footer/default.php"); ?>
  </div>

</body>
</html>
