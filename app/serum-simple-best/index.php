<!DOCTYPE html>
<html lang="ja">
<head>
  <?php include("../views/head.php"); ?>
  <link rel="stylesheet" type="text/css" media="all" href="/public/css/reset.css">
  <link rel="stylesheet" type="text/css" media="all" href="/public/css/06ssb/style.css?ver=1.0.5">
  <link rel="stylesheet" type="text/css" media="all" href="/public/css/form/style.css">
  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5HZ565Q');</script>
<!-- End Google Tag Manager -->
</head>
<body>
  <script id="tagjs" type="text/javascript">
  (function () {
    var tagjs = document.createElement("script");
    var s = document.getElementsByTagName("script")[0];
    tagjs.async = true;
    tagjs.src = "//s.yjtag.jp/tag.js#site=1FNnqKh,7LkYJz3";
    s.parentNode.insertBefore(tagjs, s);
  }());
</script>
<noscript>
  <iframe src="//b.yjtag.jp/iframe?c=1FNnqKh,7LkYJz3" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
</noscript>
  <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HZ565Q"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
  <div class="wrapper">
    <header>
      <p id="top"><img src="/public/image/06ssb00/img-00.jpg" alt=""></p>
    </header>
    <div class="container">

      <?php if (isset($_GET['search']) && $_GET['search'] == 'show') { ?>
        <?php include("../views/search.php"); ?>
      <?php } ?>

      <p><img src="/public/image/06ssb00/img-01.jpg" alt=""></p>
      <p><a class="" href="#rankFirst"><img src="/public/image/06ssb00/img-02.jpg" alt=""></a></p>
      <p><img src="/public/image/06ssb00/img-03.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-04.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-05.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-06.jpg" alt=""></p>
      <p><a class="" href="#rankFirst"><img src="/public/image/06ssb00/img-07.jpg" alt=""></a></p>
      <p><img src="/public/image/06ssb00/img-08.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-09.jpg" alt=""></p>
      <p id="rankFirst"><img src="/public/image/06ssb00/img-10.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-11.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-12.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-13.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-14.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-15.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-16.gif" alt=""></p>
      <p><img src="/public/image/06ssb00/img-17.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-18.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-19.gif" alt=""></p>
      <p><img src="/public/image/06ssb00/img-20.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-21.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-22.gif" alt=""></p>
      <p><img src="/public/image/06ssb00/img-23.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-24.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-25.jpg" alt=""></p>
      <p><a class="siteLink" href="https://n-organic.com/ad_groups/QhoZn3P7nITEF1zr?<?php echo isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] . '&' : ''; ?>partner=cl&menu=rank&lp=ssb" target="_blank"><img src="/public/image/06ssb00/img-26.jpg" alt=""></a></p>
      <p><img src="/public/image/06ssb00/img-27.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-28.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-29.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-30.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-31.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-32.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-33.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-34.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-35.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-36.jpg" alt=""></p>
      <p><a class="anotherSiteLink" id="sk-ii-1" href="https://www.sk-ii.jp/ja/product-detail.aspx?name=rna-power-radical-new-age" target="_blank"><img src="/public/image/06ssb00/img-37.jpg" alt=""></a></p>
      <p><img src="/public/image/06ssb00/img-38.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-39.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-40.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-41.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-42.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-43.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-44.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-45.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-46.jpg" alt=""></p>
      <p><a class="anotherSiteLink" id="riceforce-1" href="https://etvos.com/fs/etvos/cd-007" target="_blank"><img src="/public/image/06ssb00/img-47.jpg" alt=""></a></p>
      <p><img src="/public/image/06ssb00/img-48.jpg" alt=""></p>
      <p><a class="" href="#rankFirst"><img src="/public/image/06ssb00/img-49.jpg" alt=""></a></p>
      <p><img src="/public/image/06ssb00/img-50.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-51.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-52.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-53.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-54.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-55.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-56.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-57.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-58.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-59.jpg" alt=""></p>
      <p><a class="" href="#top"><img src="/public/image/06ssb00/img-60.jpg" alt=""></a></p>
      <p><img src="/public/image/06ssb00/img-61.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-62.jpg" alt=""></p>
      <p><img src="/public/image/06ssb00/img-63.jpg" alt=""></p>
    </div>
    <footer>
      <p><img src="/public/image/06ssb00/img-64.jpg" alt=""></p>
      <p><a class="textLink" href="/public/pdf/06ssb/survey_overview-20171102.pdf" target="_blank"><img src="/public/image/06ssb00/img-65.jpg" alt=""></a></p>
      <p><img src="/public/image/06ssb00/img-66.jpg" alt=""></p>
    </footer>
  </div>

</body>
</html>
