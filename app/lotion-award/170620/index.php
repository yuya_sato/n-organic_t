<!DOCTYPE html>
<html lang="ja">
<head>
  <?php include("../../views/head.php"); ?>
  <link rel="stylesheet" type="text/css" media="all" href="/public/css/01la/style.css">
  <link rel="stylesheet" type="text/css" media="all" href="/public/css/form/style.css">
</head>
<body>
  <div class="wrapper">
    <header>
      <p id="top"><img src="/public/image/01la00/img-01.jpg" alt=""></p>
    </header>
    <div class="container">
      <p><img src="/public/image/01la00/img-02.jpg" alt=""></p>
      <div class="flexWrapper">
        <p><img src="/public/image/01la00/img-03.jpg" alt=""></p>
        <p><a class="" href="#rankFirst"><img src="/public/image/01la00/img-04.jpg" alt=""></a></p>
      </div>

      <?php if (isset($_GET['search']) && $_GET['search'] == 'show') { ?>
        <?php include("../../views/search.php"); ?>
      <?php } ?>

      <p><img src="/public/image/01la00/img-05.jpg" alt=""></p>
      <p id="rankFirst"><img src="/public/image/01la00/img-06.jpg" alt=""></p>
      <p><img src="/public/image/01la00/img-07.jpg" alt=""></p>
      <p><a class="siteLink" href="https://n-organic.com/ads/sp04_2s00?<?php echo isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] . '&' : ''; ?>partner=cl&menu=rank&lp=la-set" target="_blank"><img src="/public/image/01la00/img-08.jpg" alt=""></a></p>
      <p><img src="/public/image/01la00/img-09.jpg" alt=""></p>
      <p><a class="anotherSiteLink" id="sk-ii-1" href="https://www.sk-ii.jp/ja/product-detail.aspx?name=facial-treatment-essence" target="_blank"><img src="/public/image/01la00/img-10.jpg" alt=""></a></p>
      <p><img src="/public/image/01la00/img-11.jpg" alt=""></p>
      <p><a class="anotherSiteLink" id="riceforce-1" href="http://www.riceforce.com/stc/item/deep/lotion.aspx" target="_blank"><img src="/public/image/01la00/img-12.jpg" alt=""></a></p>
      <p><img src="/public/image/01la00/img-13.jpg" alt=""></p>
      <div class="flexWrapper">
        <p><a class="anotherSiteLink" id="threecosmetics-1" href="http://www.threecosmetics.com/fs/three/ski-0101007" target="_blank"><img src="/public/image/01la00/img-14.jpg" alt=""></a></p>
        <p><a class="anotherSiteLink" id="albion-1" href="http://www.albion.co.jp/products/skincare/exsc_sces" target="_blank"><img src="/public/image/01la00/img-15.jpg" alt=""></a></p>
      </div>
      <p><img src="/public/image/01la00/img-16.jpg" alt=""></p>
      <div class="flexWrapper">
        <p><a class="anotherSiteLink" id="ipsa-1" href="http://www.ipsa.co.jp/" target="_blank"><img src="/public/image/01la00/img-17.jpg" alt=""></a></p>
        <p><a class="anotherSiteLink" id="pola-1" href="http://net.pola.co.jp/beauty/products/html/item/002/024/item151225.html" target="_blank"><img src="/public/image/01la00/img-18.jpg" alt=""></a></p>
      </div>
      <p><img src="/public/image/01la00/img-19.jpg" alt=""></p>
      <p><img src="/public/image/01la00/img-20.jpg" alt=""></p>
      <p><img src="/public/image/01la00/img-21.jpg" alt=""></p>
      <p><img src="/public/image/01la00/img-22.jpg" alt=""></p>
      <p><img src="/public/image/01la00/img-23.jpg" alt=""></p>
      <p><img src="/public/image/01la01/img-24.jpg" alt=""></p>
      <p><a class="siteLink" href="https://n-organic.com/ads/sp04_2s00?<?php echo isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] . '&' : ''; ?>partner=cl&menu=rank&lp=la-set" target="_blank"><img src="/public/image/01la00/img-25.jpg" alt=""></a></p>
      <p><img src="/public/image/01la00/img-26.jpg" alt=""></p>
      <p><img src="/public/image/01la00/img-27.jpg" alt=""></p>
      <p><img src="/public/image/01la00/img-28.jpg" alt=""></p>
      <p><a class="siteLink" href="https://n-organic.com/ads/sp04_2s00?<?php echo isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] . '&' : ''; ?>partner=cl&menu=rank&lp=la-set" target="_blank"><img src="/public/image/01la00/img-29.jpg" alt=""></a></p>
      <p><img src="/public/image/01la00/img-30.jpg" alt=""></p>
      <p><img src="/public/image/01la00/img-31.jpg" alt=""></p>
      <p><img src="/public/image/01la00/img-32.jpg" alt=""></p>
      <p><img src="/public/image/01la00/img-33.jpg" alt=""></p>
      <p><img src="/public/image/01la00/img-34.jpg" alt=""></p>
      <p><a class="anotherSiteLink" id="sk-ii-2" href="https://www.sk-ii.jp/ja/product-detail.aspx?name=facial-treatment-essence" target="_blank"><img src="/public/image/01la00/img-35.jpg" alt=""></a></p>
      <p><img src="/public/image/01la00/img-36.jpg" alt=""></p>
      <p><img src="/public/image/01la00/img-37.jpg" alt=""></p>
      <p><img src="/public/image/01la00/img-38.jpg" alt=""></p>
      <p><img src="/public/image/01la00/img-39.jpg" alt=""></p>
      <p><img src="/public/image/01la00/img-40.jpg" alt=""></p>
      <p><img src="/public/image/01la00/img-41.jpg" alt=""></p>
      <p><img src="/public/image/01la00/img-42.jpg" alt=""></p>
      <p><img src="/public/image/01la00/img-43.jpg" alt=""></p>
      <p><a class="anotherSiteLink" id="riceforce-2" href="http://www.riceforce.com/stc/item/deep/lotion.aspx" target="_blank"><img src="/public/image/01la00/img-44.jpg" alt=""></a></p>
      <p><img src="/public/image/01la00/img-45.jpg" alt=""></p>
      <p><img src="/public/image/01la00/img-46.jpg" alt=""></p>
      <p><img src="/public/image/01la00/img-47.jpg" alt=""></p>
      <p><img src="/public/image/01la00/img-48.jpg" alt=""></p>
      <p><a class="anotherSiteLink" id="threecosmetics-2" href="http://www.threecosmetics.com/fs/three/ski-0101007" target="_blank"><img src="/public/image/01la00/img-49.jpg" alt=""></a></p>
      <p><img src="/public/image/01la00/img-50.jpg" alt=""></p>
      <p><a class="anotherSiteLink" id="albion-2" href="http://www.albion.co.jp/products/skincare/exsc_sces" target="_blank"><img src="/public/image/01la00/img-51.jpg" alt=""></a></p>
      <p><img src="/public/image/01la00/img-52.jpg" alt=""></p>
      <p><a class="anotherSiteLink" id="ipsa-2" href="http://www.ipsa.co.jp/" target="_blank"><img src="/public/image/01la00/img-53.jpg" alt=""></a></p>
      <p><img src="/public/image/01la00/img-54.jpg" alt=""></p>
      <p><a class="anotherSiteLink" id="pola-2" href="http://net.pola.co.jp/beauty/products/html/item/002/024/item151225.html" target="_blank"><img src="/public/image/01la00/img-55.jpg" alt=""></a></p>
      <p><img src="/public/image/01la00/img-56.jpg" alt=""></p>
      <p><a class="" href="#top"><img src="/public/image/01la00/img-57.jpg" alt=""></a></p>
      <p><img src="/public/image/01la00/img-58.jpg" alt=""></p>
      <p><img src="/public/image/01la00/img-59.jpg" alt=""></p>
    </div>
    <footer>
      <p><img src="/public/image/01la00/img-65.jpg" alt=""></p>
      <p><a class="textLink" href="/public/pdf/01la/survey_overview-20170613.pdf" target="_blank"><img src="/public/image/01la00/img-66.jpg" alt=""></a></p>
      <p><img src="/public/image/01la00/img-67.jpg" alt=""></p>
    </footer>
  </div>

</body>
</html>
