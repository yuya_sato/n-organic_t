<!DOCTYPE html>
<html lang="ja">
<head>
  <?php include("../views/head.php"); ?>
  <link rel="stylesheet" type="text/css" media="all" href="/public/css/reset.css">
  <link rel="stylesheet" type="text/css" media="all" href="/public/css/02sla/style.css?ver=1.0.5">
  <link rel="stylesheet" type="text/css" media="all" href="/public/css/form/style.css">
</head>
<body>
  <div class="wrapper">
    <header>
      <p id="top"><img src="/public/image/05lsb00/img-00.jpg" alt=""></p>
    </header>
    <div class="container">

      <?php if (isset($_GET['search']) && $_GET['search'] == 'show') { ?>
        <?php include("../views/search.php"); ?>
      <?php } ?>

      <p id="rankFirst"><img src="/public/image/05lsb00/img-09.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-10.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-11.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-12.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-13.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-14.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-15.jpg" alt=""></p>
      <p><img src="/public/image/05lsb01/img-16.jpg" alt=""></p>
      <p><a class="siteLink" href="https://n-organic.com/ad_groups/lotion-short-best?&<?php echo isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] . '&' : ''; ?>partner=cl&menu=rank&lp=lshb" target="_blank"><img src="/public/image/05lsb01/img-19.jpg" alt=""></a></p>
      <p><img src="/public/image/05lsb01/img-20.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-21.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-22.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-23.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-24.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-25.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-26.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-27.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-28.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-29.jpg" alt=""></p>
      <p><a class="anotherSiteLink" id="sk-ii-1" href="https://www.sk-ii.jp/ja/product-detail.aspx?name=facial-treatment-essence" target="_blank"><img src="/public/image/05lsb00/img-30.jpg" alt=""></a></p>
      <p><img src="/public/image/05lsb00/img-31.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-32.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-33.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-34.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-35.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-36.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-37.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-38.jpg" alt=""></p>
      <p><a class="anotherSiteLink" id="riceforce-1" href="http://www.riceforce.com/stc/item/deep/lotion.aspx" target="_blank"><img src="/public/image/05lsb00/img-39.jpg" alt=""></a></p>
      <p><img src="/public/image/05lsb00/img-40.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-41.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-42.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-43.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-44.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-45.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-46.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-47.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-48.jpg" alt=""></p>
      <p><a class="" href="#top"><img src="/public/image/05lsb00/img-49.jpg" alt=""></a></p>
      <p><img src="/public/image/05lsb00/img-50.jpg" alt=""></p>
      <p><img src="/public/image/05lsb00/img-51.jpg" alt=""></p>
    </div>
    <footer>
      <p><img src="/public/image/05lsb00/img-52.jpg" alt=""></p>
      <p><a class="textLink" href="/public/pdf/02sla/survey_overview-20170711.pdf" target="_blank"><img src="/public/image/05lsb00/img-53.jpg" alt=""></a></p>
      <p><img src="/public/image/05lsb00/img-54.jpg" alt=""></p>
    </footer>
  </div>

</body>
</html>
