<div class="item">
  <p class="title"><?php echo $cosme['name'] ?></p>
  <div class="item-containts">
    <div class="item-image">
      <img src="<?php echo $cosme['image'] ?>" alt="">
    </div>
    <div class="item-table">
      <div class="item-table-tr">
        <div class="item-table-cell">獲得票</div>
        <div class="item-table-cell"><?php echo $cosme['vote'] ?></div>
      </div>
      <div class="item-table-tr">
        <div class="item-table-cell">価格</div>
        <div class="item-table-cell"><?php echo $cosme['price'] ?></div>
      </div>
      <div class="item-table-tr">
        <div class="item-table-cell">内容量</div>
        <div class="item-table-cell"><?php echo $cosme['capacity'] ?></div>
      </div>
      <div class="item-table-tr">
        <div class="item-table-cell">初回トライアル</div>
        <div class="item-table-cell"><?php echo $cosme['trial'] ? 'あり' : 'なし' ?></div>
      </div>
      <div class="item-table-tr">
        <div class="item-table-cell">定期割引</div>
        <div class="item-table-cell"><?php echo $cosme['subscription'] ? 'あり' : 'なし' ?></div>
      </div>
      <div class="item-table-tr">
        <div class="item-table-cell">送料</div>
        <div class="item-table-cell"><?php echo $cosme['postage'] ? 'あり' : 'なし' ?></div>
      </div>
      <div class="item-table-tr">
        <div class="item-table-cell">返品保証</div>
        <div class="item-table-cell"><?php echo $cosme['returned'] ? 'あり' : 'なし' ?></div>
      </div>
    </div>
  </div>
  <div class="link-button-wrapper">
    <a href="<?php echo $cosme['link'] ?>" target="_blank" class="link-button">公式サイトへ</a>
  </div>
</div>
