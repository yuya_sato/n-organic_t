    <footer class="footer">
      <div class="box-footer">
        <dl class="footer-lists">
          <dt class="footer-item_text">運営者</dt>
          <dd class="footer-item_text">株式会社AROUND</dd>
          <dt class="footer-item_text">所在地</dt>
          <dd class="footer-item_text">東京都渋谷区神南1-5-14</dd>
          <dt class="footer-item_text">お問い合わせ</dt>
          <dd class="footer-item_text">cosmelabo2018@gmail.com</dd>
        </dl>
        <div class="footer-license">
          <small class="footer-item_text">&copy;AROUND INC. 2017</small>
        </div>
      </div>
    </footer>
