  <form class="search-wrapper" action="/search" method="get" style="margin-top: 10px;">
    <p class="search-title">絞り込み検索</p>
    <div class="from-wrapper">
      <p class="from-cell-title">獲得票</p>
      <div class="from-cell-group">
        <input type="radio" id="vote_all" value="vote_all" checked name="vote"><label for="vote_all">全て</label>
      </div>
      <div class="from-cell-group">
        <input type="radio" id="vote_500" value="500" name="vote"><label for="vote_500">~ 500</label>
      </div>
      <div class="from-cell-group">
        <input type="radio" id="vote_1000" value="1000" name="vote"><label for="vote_1000">~ 1000</label>
      </div>
      <div class="from-cell-group">
        <input type="radio" id="vote_2000" value="2000" name="vote"><label for="vote_2000">~ 2000</label>
      </div>
    </div>

    <div class="from-wrapper">
      <p class="from-cell-title">価格帯</p>
      <div class="from-cell-group">
        <input type="radio" id="price_all" value="price_all" checked name="price"><label for="price_all">全て</label>
      </div>
      <div class="from-cell-group">
        <input type="radio" id="price_3000" value="3000" name="price"><label for="price_3000">~ 3,000円</label>
      </div>
      <div class="from-cell-group">
        <input type="radio" id="price_5000" value="5000" name="price"><label for="price_5000">~ 5,000円</label>
      </div>
      <div class="from-cell-group">
        <input type="radio" id="price_20000" value="20000" name="price"><label for="price_20000">~ 20,000円</label>
      </div>
    </div>

    <div class="from-wrapper">
      <p class="from-cell-title">内容量</p>
      <div class="from-cell-group">
        <input type="radio" id="capacity_all" value="capacity_all" checked name="capacity"><label for="capacity_all">全て</label>
      </div>
      <div class="from-cell-group">
        <input type="radio" id="capacity_100ml" value="100" name="capacity"><label for="capacity_100ml">~ 100ml</label>
      </div>
      <div class="from-cell-group">
        <input type="radio" id="capacity_150ml" value="150" name="capacity"><label for="capacity_150ml">~ 150ml</label>
      </div>
      <div class="from-cell-group">
        <input type="radio" id="capacity_200ml" value="200" name="capacity"><label for="capacity_200ml">~ 200ml</label>
      </div>
    </div>

    <div class="from-wrapper">
      <p class="from-cell-title">初回トライアル</p>
      <div class="from-cell-group">
        <input type="radio" id="trial_all" value="trial_all" checked name="trial"><label for="trial_all">全て</label>
      </div>
      <div class="from-cell-group">
        <input type="radio" id="trial_true" value="true" name="trial"><label for="trial_true">あり</label>
      </div>
      <div class="from-cell-group">
        <input type="radio" id="trial_false" value="false" name="trial"><label for="trial_false">なし</label>
      </div>
    </div>

    <div class="from-wrapper">
      <p class="from-cell-title">定期割引</p>
      <div class="from-cell-group">
        <input type="radio" id="subscription_all" value="subscription_all" checked name="subscription"><label for="subscription_all">全て</label>
      </div>
      <div class="from-cell-group">
        <input type="radio" id="subscription_true" value="true" name="subscription"><label for="subscription_true">あり</label>
      </div>
      <div class="from-cell-group">
        <input type="radio" id="subscription_false" value="false" name="subscription"><label for="subscription_false">なし</label>
      </div>
    </div>

    <div class="from-wrapper">
      <p class="from-cell-title">送料</p>
      <div class="from-cell-group">
        <input type="radio" id="postage_all" value="postage_all" checked name="postage"><label for="postage_all">全て</label>
      </div>
      <div class="from-cell-group">
        <input type="radio" id="postage_true" value="true" name="postage"><label for="postage_true">あり</label>
      </div>
      <div class="from-cell-group">
        <input type="radio" id="postage_false" value="false" name="postage"><label for="postage_false">なし</label>
      </div>
    </div>

    <div class="from-wrapper">
      <p class="from-cell-title">返品保証</p>
      <div class="from-cell-group">
        <input type="radio" id="returned_all" value="returned_all" checked name="returned"><label for="returned_all">全て</label>
      </div>
      <div class="from-cell-group">
        <input type="radio" id="returned_true" value="true" name="returned"><label for="returned_true">あり</label>
      </div>
      <div class="from-cell-group">
        <input type="radio" id="returned_false" value="false" name="returned"><label for="returned_false">なし</label>
      </div>
    </div>

    <div class="from-wrapper">
      <p class="from-cell-title">並び順</p>
      <div class="from-cell-group">
        <input type="radio" id="sort_vote" value="vote" checked name="sort"><label for="sort_vote">獲得票</label>
      </div>
      <div class="from-cell-group">
        <input type="radio" id="sort_price" value="price" name="sort"><label for="sort_price">価格</label>
      </div>
      <div class="from-cell-group">
        <input type="radio" id="sort_capacity" value="capacity" name="sort"><label for="sort_capacity">内容量</label>
      </div>
    </div>

    <div class="search-submit-button-wrapper">
      <input type="submit" class="search-submit-button" value="検索">
    </div>
  </form>
