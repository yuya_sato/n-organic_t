$(window).on('load', function() {
  // ページ初回スクロール時の動作(リタゲ用)
  $(window).one('scroll', function () {
    if (typeof dataLayer != 'undefined') {
      dataLayer.push({'event': 'scroll'});
    }
  });

  // CVタグ
  $('.siteLink').on('click', function() {
    if (typeof dataLayer != 'undefined') {
      dataLayer.push({'event': 'siteLink'});
    }
  })

  // 他のCVタグ
  $('.anotherSiteLink').on('click', function() {
    if (typeof dataLayer != 'undefined') {
      dataLayer.push({'event': 'anotherSiteLink-' + $(this).attr("id")});
    }
  })

  // タグの発火を1秒間待って本サイトに遷移する
  $('a.siteLink').on('click', function (e) {
      var redirectUrl = $(this).attr('href');
      setTimeout(function () {
          location.href = redirectUrl;
      }, 1000);
      e.preventDefault();
  });
})
