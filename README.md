# cosme-magazine サーバー

## commandの使い方

`command` コマンドによって、アプリケーションのビルドや実行をできます。

```bash
./command {run}
```

### 操作

* run: WEBサーバを起動します。
  - WEBサーバには、[http://localhost:8000/](http://localhost:8000/)でアクセスすることができます。
